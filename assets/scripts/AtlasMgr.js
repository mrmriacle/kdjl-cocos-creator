var AtlasMgr = {
    atDic: new Array(),
    loadRes: function(sptAtlasPath){
        var self = this;
        cc.loader.loadRes("res/" + sptAtlasPath, cc.SpriteAtlas, function (err, atlas) {         
            self.atDic[sptAtlasPath] = atlas;
            cc.log(err);
        });
    },
    getSpt: function(sptAtlasPath, sptName) {
        if (this.atDic[sptAtlasPath] != null){
            var atlas = this.atDic [sptAtlasPath];
            return atlas.getSpriteFrame(sptAtlasPath);
        }
    },
    delAtals: function(sptAtlasPath){
        this.atDic[sptAtlasPath] = null;
    },
};

module.exports = AtlasMgr;