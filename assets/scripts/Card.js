cc.Class({
    extends: cc.Component,

    properties: {
        textCardNum: cc.Label,
        imgBigCardType: cc.Sprite,
        imgSmCardType: cc.Sprite,
        transMove: cc.Node,
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
        this.initParas();
        this.initEvents();
    },

    update (dt) {
       
    },

    initParas: function(){
        this._iCardType;         // 1-4方块、梅花、红桃、黑桃
        this._iCardNum;          // 1-13
        this._iColor;            // 0红、1黑
        this._iPos;              // 0牌组、1中转、2终点
        this._iRow;              // 0-7
        this.items = [];
        this.adMgr = cc.find("Mgr").getComponent("AudioMgr");
        this._bDown = false;
    },

    playWinMove: function(){
        this._bPlayWin = true;
    },

    init: function(iCardNum, iCardType, iPos, iRow, delt){
        this.node.active = true;
        this._iPos = iPos;
        this._iRow = iRow;
        this._bMove = false;
        this._delegate = delt;

        this._iCardType = iCardType;
        this._iCardNum = iCardNum;
        this._iColor = iCardType % 2 == 1 ? 0 : 1;
        this.textCardNum.string = this.getSCardNum (iCardNum);
        this.textCardNum.node.color = iCardType % 2 == 1 ? (new cc.Color(160, 50, 40)) : cc.Color.BLACK;
        var tCardType = ["fangkuai", "meihua", "hongtao", "heitao"];
        var self = this;
        cc.loader.loadRes("res/" + tCardType[iCardType - 1], cc.SpriteFrame, function (err, SpriteFrame) {
            self.imgBigCardType.spriteFrame = SpriteFrame;
            self.imgSmCardType.spriteFrame = SpriteFrame;
        });
    },

    getSCardNum: function (iCard) {
        var s = 'AJQK', sCard;
        if (iCard == 1)
            sCard = s[0];
        else if (iCard > 10)
            sCard = s[iCard - 10];
        else
            sCard = iCard;
        return sCard;
    },

    initEvents: function(){
        //touchstart mousedown
        //touchmove mousemove
        //touchend mouseup
        //touchcancel
        this.node.on("touchstart", function (event) {
            if (this._bMove == false)
                return;
            this._bDown = true;
        }, this);
        this.node.on("touchmove", function (event) {
            if (this._bMove == false || this._bDown == false)
                return;
            if (this.node.parent != this.transMove){
                this.addItems ();
                this.node.parent = this.transMove;
                // this.node.setPosition(event._x, event._y);
                this.node.setPosition(event.getLocation());
                var items = this.items;
                if (items.length > 0){
                    var idx = 0;
                    for (var i = 0; i < items.length; i++) {
                        idx++;
                        items[i].parent = this.node;
                        items[i].setPosition(0, -30 * idx);
                    };
                }
            }
            // this.node.setPosition(event._x, event._y);
            this.node.setPosition(event.getLocation());
        }, this);
        this.node.on("touchend", function (event) {
            if (this._bMove == false || this._bDown == false)
                return;
            if (this.node.parent == this.transMove){
                var bDrop = this._delegate.getBDropped (this, event);
                if (bDrop == false){
                    this.adMgr.PlaySound("click");
                    var transP = this._delegate.getTransP (this._iPos, this._iRow);
                    this.node.parent = transP;
                    this.node.setPosition(0, 0 - 30 * (transP.childrenCount - 1));
                    if (this.node.childrenCount > 3) {
                        var children = this.node.children;
                        for (var i = 3, len = children.length; i < len; i++) {
                            var item = children[3];
                            item.parent = transP;
                            item.setPosition(0, 0 - 30 * (transP.childrenCount - 1), 0);
                        }
                    }
                }
            } else {
                this.addItems ();
                if (this.items.length == 0 && (this._delegate.getBMoveToTotalDes (this) == true || this._delegate.getBMoveToCard (this, true) == true
                    || this._delegate.getBMoveToMedium (this) == true)) {

                } else if (this._delegate.getBMoveToCard (this, true) == true) {

                } else {
                    this.adMgr.PlaySound("click");
                }
            }
            if (this._iPos != 0) {
                this.node.setPosition(0, 0);
            }
            this._bDown = false;
        }, this);
        this.node.on("touchcancel", function (event) {
            if (this._bMove == false || this._bDown == false)
                return;
            if (this.node.parent == this.transMove){
                var bDrop = this._delegate.getBDropped (this, event);
                if (bDrop == false){
                    this.adMgr.PlaySound("click");
                    var transP = this._delegate.getTransP (this._iPos, this._iRow);
                    this.node.parent = transP;
                    this.node.setPosition(0, 0 - 30 * (transP.childrenCount - 1));
                    if (this.node.childrenCount > 3) {
                        var children = this.node.children;
                        for (var i = 3, len = children.length; i < len; i++) {
                            var item = children[3];
                            item.parent = transP;
                            item.setPosition(0, 0 - 30 * (transP.childrenCount - 1), 0);
                        }
                    }
                }
            } else {
                this.addItems ();
                if (this.items.length == 0 && (this._delegate.getBMoveToTotalDes (this) == true || this._delegate.getBMoveToCard (this, true) == true
                    || this._delegate.getBMoveToMedium (this) == true)) {

                } else if (this._delegate.getBMoveToCard (this, true) == true) {

                } else {
                    this.adMgr.PlaySound("click");
                }
            }
            if (this._iPos != 0) {
                this.node.setPosition(0, 0);
            }
            this._bDown = false;
        }, this);
    },

    setBMove: function(bMove){
        this._bMove = bMove;
    },

    getBMove: function(){
        return this._bMove;
    },

    getPos: function(){
        return this._iPos;
    },

    getRow: function(){
        return this._iRow;
    },

    getColor: function(){
        return this._iColor;
    },

    getCardType: function(){
        return this._iCardType;
    },

    getCardNum: function(){
        return this._iCardNum;
    },

    setPos: function(pos){
        this._iPos = pos;
    },

    setRow: function(iRow){
        this._iRow = iRow;
    },

    getItems: function(){
        return this.items;
    },

    addItems: function(){
        this.items = [];
        if (this._iPos == 0){
            var trans = this.node.parent;
            var len = trans.childrenCount;
            var children = trans.children;
            var idx = this.node.getSiblingIndex();
            if (len > 0) {
                for (var i = idx + 1; i < len; i++) {
                    this.items.push(children[i]);
                }
            }
        }
    },
});
